
package net.servodata.robber.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ClientConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("allegro.wsdl");
		return marshaller;
	}

	@Bean
	public AukroClient weatherClient(Jaxb2Marshaller marshaller) {
		AukroClient client = new AukroClient();
		client.setDefaultUri("https://webapi.allegro.pl/service.php");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}
