package net.servodata.robber.client;

import allegro.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class AukroClient extends WebServiceGatewaySupport {

	public final DoQuerySysStatusResponse doQuerySysStatusRequestTest() {
		DoQuerySysStatusRequest request = new DoQuerySysStatusRequest();

		request.setCountryId(56);
		request.setWebapiKey("97a8d9ad");
		request.setSysvar(1);

		return (DoQuerySysStatusResponse) getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", request);
	}

	public final DoLoginResponse doLogin(String username, String password, int countryCode, String webapiKey, long localVersionKey) {
		DoLoginRequest doLoginRequest = new DoLoginRequest();

		doLoginRequest.setUserLogin(username);
		doLoginRequest.setUserPassword(password);
		doLoginRequest.setCountryCode(countryCode);
		doLoginRequest.setWebapiKey(webapiKey);
		doLoginRequest.setLocalVersion(localVersionKey);
		
		return (DoLoginResponse) getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", doLoginRequest);
	}

	public final DoGetCatsDataResponse doGetCatsData(int countryCode, long localVersionKey, String webApiKey) {
		DoGetCatsDataRequest request = new DoGetCatsDataRequest();

		request.setCountryId(countryCode);
		request.setLocalVersion(localVersionKey);
		request.setWebapiKey(webApiKey);
		
		return (DoGetCatsDataResponse) getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", request);
	}

	public final DoGetSellFormFieldsExtResponse doGetSellFormFieldsExt(int countryCode, long localVersionKey, String webApiKey) {
		DoGetSellFormFieldsExtRequest request = new DoGetSellFormFieldsExtRequest();
		request.setCountryCode(countryCode);
		request.setLocalVersion(localVersionKey);
		request.setWebapiKey(webApiKey);

		return (DoGetSellFormFieldsExtResponse) getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", request);
	}

	public final DoGetItemsListResponse doGetItemsList(int countryCode, String webApiKey) {
		DoGetItemsListRequest request = new DoGetItemsListRequest();
		request.setCountryId(countryCode);
		request.setWebapiKey(webApiKey);
		request.setFilterOptions(new ArrayOfFilteroptionstype());
		FilterOptionsType filterOptionsType = new FilterOptionsType();
		filterOptionsType.setFilterId("special");
		filterOptionsType.setFilterValueId(new ArrayOfString());
		//nejnovejsi
		filterOptionsType.getFilterValueId().getItem().add("new");
		//Elektronika - > Mobily a GPS -> Mobilní telefony -> Apple
		//filterOptionsType.getFilterValueId().getItem().add("107152");
		request.getFilterOptions().getItem().add(filterOptionsType);

		request.setResultSize(1000);

		Object result = getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", request);
		return (DoGetItemsListResponse) result;
	}

	public final DoShowItemInfoExtResponse doShowItemInfoExt(String sessionHandle, Long itemId) {
		try {
			DoShowItemInfoExtRequest request = new DoShowItemInfoExtRequest();
			request.setSessionHandle(sessionHandle);
			request.setItemId(itemId);
			request.setGetDesc(1);
			request.setGetImageUrl(1);
			request.setGetAttribs(1);
			request.setGetPostageOptions(1);
			request.setGetCompanyInfo(1);
			request.setGetProductInfo(1);

			Object result = getWebServiceTemplate().marshalSendAndReceive("https://webapi.allegro.pl/service.php", request);

			return (DoShowItemInfoExtResponse) result;
		} catch (Exception e) {
			System.err.println(e.toString());
			return null;
		}
	}
}
