package net.servodata.robber;

import allegro.wsdl.*;
import net.servodata.robber.bo.ItemAttributeBo;
import net.servodata.robber.bo.ItemCategoryBo;
import net.servodata.robber.converter.AttributeConverter;
import net.servodata.robber.converter.CategoryConverter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import net.servodata.robber.client.AukroClient;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
public class AukroDataRobberApplication {

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    private Map<Integer, ItemCategoryBo> catMap = new HashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(AukroDataRobberApplication.class);
    }

    @Bean
    CommandLineRunner lookup(AukroClient aukroClient) {

        return args -> {
            int countryCode = 56;
            String webApiKey = "97a8d9ad";

            DoQuerySysStatusResponse doQuerySysStatusResponse = aukroClient.doQuerySysStatusRequestTest();
            long localVersionKey = doQuerySysStatusResponse.getVerKey();

            DoLoginResponse doLoginResponse = aukroClient.doLogin("tester-b", "ProjektA1", countryCode, webApiKey, localVersionKey);
            String sessionHandle = doLoginResponse.getSessionHandlePart();

            DoGetCatsDataResponse doGetCatsDataCountResponse = aukroClient.doGetCatsData(countryCode, localVersionKey, webApiKey);
            List<ItemCategoryBo> itemCategoryBos = processDoGetCatsDataResponse(doGetCatsDataCountResponse, true);

            DoGetSellFormFieldsExtResponse doGetSellFormFieldsExtResponse = aukroClient.doGetSellFormFieldsExt(countryCode, localVersionKey, webApiKey);
            processDoGetSellFormFieldsExtResponse(doGetSellFormFieldsExtResponse, itemCategoryBos);

            DoGetItemsListResponse doGetItemsListResponse = aukroClient.doGetItemsList(countryCode, webApiKey);
            Map<Long, ItemsListType> itemIds = processDoGetItemsListResponse(doGetItemsListResponse);

            List<DoShowItemInfoExtResponse> itemInfos = new ArrayList<>();
            for (Long itemId : itemIds.keySet()) {
                DoShowItemInfoExtResponse doShowItemInfoExtResponse = aukroClient.doShowItemInfoExt(sessionHandle, itemId);
                if(doShowItemInfoExtResponse != null) {
                    itemInfos.add(doShowItemInfoExtResponse);
                }
            }
            processDoShowItemInfoExtResponses(itemInfos, itemIds);
        };
    }

    private List<ItemCategoryBo> processDoGetCatsDataResponse(DoGetCatsDataResponse response, boolean save) {
        String insertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_01__ItemCategory_testData.sql";
        File insert = new File(insertPath);
        List<ItemCategoryBo>result = new ArrayList<>();
        try {
            if (!insert.exists()) {
                insert.createNewFile();
            }
            FileWriter fw = new FileWriter(insert.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            CategoryConverter converter = new CategoryConverter();

            if(save) {
                /*
                bw.write("alter table item_category disable trigger all;");
                bw.newLine();
                */
                bw.write("ALTER TABLE item_category DROP CONSTRAINT FK_ic_ParentId;");
                bw.newLine();
            }
            for (CatInfoType type : response.getCatsList().getItem()) {
                ItemCategoryBo itemCategoryBo = converter.convert(type);
                boolean leaf = true;
                for (CatInfoType catInfoType : response.getCatsList().getItem()) {
                    if (catInfoType.getCatParent() == type.getCatId()) {
                        leaf = false;
                        break;
                    }
                }
                itemCategoryBo.setLeaf(leaf);
                if(save) {
                    bw.write("INSERT INTO item_category (id, name, parent_id, root, leaf, active, position, create_time, create_by, modify_time, modify_by) values(" +
                            itemCategoryBo.getId() + ", '" +
                            itemCategoryBo.getName() + "', " +
                            itemCategoryBo.getParentId() + ", " +
                            itemCategoryBo.getRoot() + ", " +
                            itemCategoryBo.getLeaf() + ", " +
                            itemCategoryBo.getActive() + ", " +
                            itemCategoryBo.getPosition() + ", '" +
                            dtf.format(itemCategoryBo.getCreateTime()) + "', '" +
                            itemCategoryBo.getCreatedBy() + "', '" +
                            dtf.format(itemCategoryBo.getModifyTime()) + "', '" +
                            itemCategoryBo.getModifiedBy() +
                            "');");
                    bw.newLine();
                }
                result.add(itemCategoryBo);
            }
            for (ItemCategoryBo cat : result) {
                catMap.put(cat.getId().intValue(), cat);
                if(cat.getParentId() == null) {
                    continue;
                }
                for (ItemCategoryBo catBo : result) {
                    /*if(cat.getId() == 107152L && catBo.getId() == 100838L) {
                        System.out.println("Apple");
                    }*/
                    if(cat.getParentId().equals(catBo.getId())) {
                        cat.setParent(catBo);
                        break;
                    }
                }
            }
            if(save) {
                //bw.write("alter table item_category enable trigger all;");
                bw.write("ALTER TABLE item_category ADD CONSTRAINT FK_ic_ParentId FOREIGN KEY (parent_id) REFERENCES item_category (id) ON DELETE No Action ON UPDATE No Action;");
            }
            bw.close();
            System.out.println("Kategorie vytvoreny");
            return result;
        } catch (IOException e) {
            System.err.println(e.toString());
            return new ArrayList<>();
        }
    }

    private void processDoGetSellFormFieldsExtResponse(DoGetSellFormFieldsExtResponse response, List<ItemCategoryBo> categories) {
        String attributeInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_02__ItemAttribute_testData.sql";
        String listValInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_03__ItemAttributeListValue_testData.sql";
        String categoryAttributeInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_04__ItemCategoryAttribute_testData.sql";
        String categoryAttributeInheritedInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_05__ItemCategoryAttributeInherited_testData.sql";
        File attributeInsert = new File(attributeInsertPath);
        File listValInsert = new File(listValInsertPath);
        File categoryAttributeInsert = new File(categoryAttributeInsertPath);
        File categoryAttributeInheritedInsert = new File(categoryAttributeInheritedInsertPath);
        try {
            if (!attributeInsert.exists()) {
                attributeInsert.createNewFile();
            }
            if (!listValInsert.exists()) {
                listValInsert.createNewFile();
            }
            if (!categoryAttributeInsert.exists()) {
                categoryAttributeInsert.createNewFile();
            }
            if (!categoryAttributeInheritedInsert.exists()) {
                categoryAttributeInheritedInsert.createNewFile();
            }

            List<String> attributeLines = new ArrayList<>();
            List<String> listValLines = new ArrayList<>();
            List<String> categoryAttributeLines = new ArrayList<>();
            List<String> categoryAttributeInheritedLines = new ArrayList<>();

            FileWriter fwAttribute = new FileWriter(attributeInsert.getAbsoluteFile());
            FileWriter fwListVal = new FileWriter(listValInsert.getAbsoluteFile());
            FileWriter fwCategoryAttribute = new FileWriter(categoryAttributeInsert.getAbsoluteFile());
            FileWriter fwCategoryAttributeInherited = new FileWriter(categoryAttributeInheritedInsert.getAbsoluteFile());

            BufferedWriter bwAttribute = new BufferedWriter(fwAttribute);
            BufferedWriter bwListVal = new BufferedWriter(fwListVal);
            BufferedWriter bwCategoryAttribute = new BufferedWriter(fwCategoryAttribute);
            BufferedWriter bwCategoryAttributeInherited = new BufferedWriter(fwCategoryAttributeInherited);

            //vypnuti kontroly klicu
            /*
            bwAttribute.write("alter table item_attribute disable trigger all;");
            bwListVal.write("alter table item_attribute_list_value disable trigger all;");
            bwCategoryAttribute.write("alter table item_category_attribute disable trigger all;");
            bwCategoryAttributeInherited.write("alter table item_category_attribute_inherited disable trigger all;");

            bwAttribute.newLine();
            bwListVal.newLine();
            bwCategoryAttribute.newLine();
            bwCategoryAttributeInherited.newLine();
            */

            AttributeConverter attributeConverter = new AttributeConverter();

            List<ItemAttributeBo> attributes = new ArrayList<>();
            Map<Long, ItemAttributeBo> createdAttributes = new HashMap<>();
            for (SellFormType sellFormType : response.getSellFormFields().getItem()) {
                if(sellFormType.getSellFormParamId() == 0) {
                    continue;
                }
                //item attribute je v podstate item category attribute, protoze si nese i kategorii, musi se proto vytvorit vsechny
                ItemAttributeBo itemAttributeBo = attributeConverter.convert(sellFormType);
                attributes.add(itemAttributeBo);

                String categoryAttributeLine = "INSERT INTO item_category_attribute (category_id, atribute_id, create_time, create_by, modify_time, modify_by) VALUES (" +
                        itemAttributeBo.getCategoryId() + ", " +
                        itemAttributeBo.getId() + ", '" +
                        dtf.format(itemAttributeBo.getCreateTime()) + "', '" +
                        itemAttributeBo.getCreatedBy() + "', '" +
                        dtf.format(itemAttributeBo.getModifyTime()) + "', '" +
                        itemAttributeBo.getModifiedBy() +
                        "');";

                categoryAttributeLines.add(categoryAttributeLine);
                bwCategoryAttribute.write(categoryAttributeLine);
                bwCategoryAttribute.newLine();

                //attributy muzou byt v db ovsem pouze jednou
                if(createdAttributes.containsKey(itemAttributeBo.getId())) {
                    continue;
                }
                createdAttributes.put(itemAttributeBo.getId(), itemAttributeBo);

                String attributeLine = "INSERT INTO item_attribute (id, type_id, name, defaultvalue, required, position, length, range_min, range_max, description, unit, create_time, create_by, modify_time, modify_by) VALUES (" +
                        itemAttributeBo.getId() + ", " +
                        itemAttributeBo.getType() + ", '" +
                        itemAttributeBo.getName() + "', '" +
                        itemAttributeBo.getDefaultValue() + "', " +
                        itemAttributeBo.isRequired() + ", " +
                        itemAttributeBo.getPosition() + ", " +
                        itemAttributeBo.getLength() + ", " +
                        itemAttributeBo.getRangeMin() + ", " +
                        itemAttributeBo.getRangeMax() + ", '" +
                        itemAttributeBo.getDescription() + "', '" +
                        itemAttributeBo.getUnit() + "', '" +
                        dtf.format(itemAttributeBo.getCreateTime()) + "', '" +
                        itemAttributeBo.getCreatedBy() + "', '" +
                        dtf.format(itemAttributeBo.getModifyTime()) + "', '" +
                        itemAttributeBo.getModifiedBy() +
                        "');";
                attributeLines.add(attributeLine);
                bwAttribute.write(attributeLine);
                bwAttribute.newLine();


                if(sellFormType.getSellFormType() == 4 || sellFormType.getSellFormType() == 5 || sellFormType.getSellFormType() == 6) {
                    //todo chtelo by to jeste intove hodnoty k jednotlivym hodnotam
                    String[] listValueValues = sellFormType.getSellFormOptsValues().split("/|");
                    String[] listValueNames = sellFormType.getSellFormDesc().split("/|");
                    if(listValueValues.length == listValueNames.length) {
                        for (int i = 0; i < listValueValues.length; i++) {
                            String listValLine = "INSERT INTO item_attribute_list_value (attribute_id, name, active, description, create_time, create_by, modify_time, modify_by) VALUES (" +
                                    itemAttributeBo.getId() + ", '" +
                                    StringEscapeUtils.escapeSql(listValueValues[i]) + "', " +
                                    "true, '" +
                                    StringEscapeUtils.escapeSql(listValueNames[i]) + "', '" +
                                    dtf.format(itemAttributeBo.getCreateTime()) + "', '" +
                                    itemAttributeBo.getCreatedBy() + "', '" +
                                    dtf.format(itemAttributeBo.getModifyTime()) + "', '" +
                                    itemAttributeBo.getModifiedBy() +
                                    "');";
                            listValLines.add(listValLine);
                            bwListVal.write(listValLine);
                            bwListVal.newLine();
                        }
                    }
                }
            }


            Map<Long, List<ItemAttributeBo>> categoryAttributes = new HashMap<>();
            for (ItemCategoryBo category : categories) {
                List<ItemAttributeBo> categoryAttrs = attributes.stream().filter(itemAttributeBo -> category.getId().equals(itemAttributeBo.getCategoryId())).collect(Collectors.toList());
                categoryAttributes.put(category.getId(), categoryAttrs);
            }
            /* pro zakladni attributy se berou z itemu a dalsich tabulek
            for (ItemAttributeBo wsFormFieldBo : attributes) {
                if (wsFormFieldBo.getItemCategory() == null) {
                    if (!categoryAttributes.containsKey(0L)) {
                        categoryAttributes.put(0L, new ArrayList<>());
                    }
                    categoryAttributes.get(0L).add(wsFormFieldBo);
                }
            }*/
            for (ItemCategoryBo category : categories) {
                if(category.getId() == 107152L) {
                    System.out.println("Apple");
                }
                List<ItemAttributeBo> categoryFormFieldsAll = getCategoryAllFields(category, categoryAttributes);
                for (ItemAttributeBo wsFormFieldBo : categoryFormFieldsAll) {
                    String categoryAttributeInheritedLine = "INSERT INTO item_category_attribute_inherited (category_id, category_atribute_id) VALUES (" +
                            category.getId() + ", " +
                            "(SELECT id FROM item_category_attribute WHERE category_id = " + wsFormFieldBo.getCategoryId() + " AND atribute_id = " + wsFormFieldBo.getId() +
                            "));";
                    categoryAttributeInheritedLines.add(categoryAttributeInheritedLine);
                    bwCategoryAttributeInherited.write(categoryAttributeInheritedLine);
                    bwCategoryAttributeInherited.newLine();
                }
            }

            //zapnuti kontroly klicu
            /*
            bwAttribute.write("alter table item_attribute enable trigger all;");
            bwListVal.write("alter table item_attribute_list_value enable trigger all;");
            bwCategoryAttribute.write("alter table item_category_attribute enable trigger all;");
            bwCategoryAttributeInherited.write("alter table item_category_attribute_inherited enable trigger all;");
            */

            bwAttribute.close();
            bwListVal.close();
            bwCategoryAttribute.close();
            bwCategoryAttributeInherited.close();
            System.out.println("Attributy vytvoreny");
        } catch (IOException e) {
            System.err.println(e.toString());
        }
    }

    private List<ItemAttributeBo> getCategoryAllFields(ItemCategoryBo category, Map<Long, List<ItemAttributeBo>> categoryFormFields) {
        List<ItemAttributeBo> result = new ArrayList<>();
        result.addAll(categoryFormFields.get(category.getId()));
        if (category.getParent() != null) {
            result.addAll(getCategoryAllFields(category.getParent(), categoryFormFields));
        }
        return result;
    }

    private Map<Long, ItemsListType> processDoGetItemsListResponse(DoGetItemsListResponse response) {
        Map<Long, ItemsListType> result = new HashMap<>();
        if(response.getItemsCount() > 0) {
            List<Long> usedCats = new ArrayList<>();
            for (ItemsListType itemsListType : response.getItemsList().getItem()) {
                System.out.println("User " + itemsListType.getSellerInfo().getUserLogin() + ": " + itemsListType.getItemTitle() + " in cat " + catMap.get(itemsListType.getCategoryId()).getName());
                if(!usedCats.contains(catMap.get(itemsListType.getCategoryId()).getId())) {
                    usedCats.add(catMap.get(itemsListType.getCategoryId()).getId());
                    result.put(itemsListType.getItemId(), itemsListType);
                }
            }
            System.out.println("Total " + usedCats.size() + " cats.");
            for (Long usedCat : usedCats) {
                System.out.println(catMap.get(usedCat.intValue()).getFullPath());
            }
        } else {
            System.out.println("No items");
        }
        return result;
    }

    private void processDoShowItemInfoExtResponses(List<DoShowItemInfoExtResponse> doShowItemInfoExtResponses, Map<Long, ItemsListType> itemIds) {
        String accountInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_06__Account_testData.sql";
        String userInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_07__User_testData.sql";
        String itemInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_08__Item_testData.sql";
        String descriptionInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_09__ItemDescription_testData.sql";
        String valueInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_10__ItemAttributeValue_testData.sql";
        String shippingInsertPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_11__ItemShippingOption_testData.sql";
        String allPath = "C:/Projects/ProjectA/app/app-db/src/test/resources/db/app_data/V003_12__ItemAll_testData.sql";

        File accountInsert = new File(accountInsertPath);
        File userInsert = new File(userInsertPath);
        File itemInsert = new File(itemInsertPath);
        File descriptionInsert = new File(descriptionInsertPath);
        File valueInsert = new File(valueInsertPath);
        File shippingInsert = new File(shippingInsertPath);
        File allLines = new File(allPath);
        try {
            if (!accountInsert.exists()) {
                accountInsert.createNewFile();
            }
            if (!userInsert.exists()) {
                userInsert.createNewFile();
            }
            if (!itemInsert.exists()) {
                itemInsert.createNewFile();
            }
            if (!descriptionInsert.exists()) {
                descriptionInsert.createNewFile();
            }
            if (!valueInsert.exists()) {
                valueInsert.createNewFile();
            }
            if (!shippingInsert.exists()) {
                shippingInsert.createNewFile();
            }
            if (!allLines.exists()) {
                allLines.createNewFile();
            }

            List<String> accountLines = new ArrayList<>();
            List<String> userLines = new ArrayList<>();
            List<String> itemLines = new ArrayList<>();
            List<String> descriptionLines = new ArrayList<>();
            List<String> valueLines = new ArrayList<>();
            List<String> shippingLines = new ArrayList<>();

            FileWriter fwAccount = new FileWriter(accountInsert.getAbsoluteFile());
            FileWriter fwUser = new FileWriter(userInsert.getAbsoluteFile());
            FileWriter fwItem = new FileWriter(itemInsert.getAbsoluteFile());
            FileWriter fwDescription = new FileWriter(descriptionInsert.getAbsoluteFile());
            FileWriter fwValue = new FileWriter(valueInsert.getAbsoluteFile());
            FileWriter fwShipping = new FileWriter(shippingInsert.getAbsoluteFile());

            BufferedWriter bwAccount = new BufferedWriter(fwAccount);
            BufferedWriter bwUser = new BufferedWriter(fwUser);
            BufferedWriter bwItem = new BufferedWriter(fwItem);
            BufferedWriter bwDescription = new BufferedWriter(fwDescription);
            BufferedWriter bwValue = new BufferedWriter(fwValue);
            BufferedWriter bwShipping = new BufferedWriter(fwShipping);

            //vypnuti kontroly klicu
            /*
            bwAccount.write("alter table account disable trigger all;");
            bwUser.write("alter table user_ disable trigger all;");
            bwItem.write("alter table item disable trigger all;");
            bwDescription.write("alter table item_desc disable trigger all;");
            bwValue.write("alter table item_attribute_value disable trigger all;");
            bwShipping.write("alter table item_shipping_option disable trigger all;");

            bwAccount.newLine();
            bwUser.newLine();
            bwItem.newLine();
            bwDescription.newLine();
            bwValue.newLine();
            bwShipping.newLine();
            */

            List<Long> userIds = new ArrayList<>();
            for (DoShowItemInfoExtResponse doShowItemInfoExtResponse : doShowItemInfoExtResponses) {

                //Seller
                addUser(userIds, doShowItemInfoExtResponse.getItemListInfoExt().getItSellerId(), accountLines, bwAccount, doShowItemInfoExtResponse.getItemListInfoExt().getItSellerLogin(), doShowItemInfoExtResponse.getItemListInfoExt().getItSellerRating(), userLines, bwUser);

                //Highest bidder
                addUser(userIds, Long.valueOf(doShowItemInfoExtResponse.getItemListInfoExt().getItHighBidder()), accountLines, bwAccount, doShowItemInfoExtResponse.getItemListInfoExt().getItHighBidderLogin(), 0, userLines, bwUser);

                //Item
                String itemLine = "INSERT INTO item(" +
                        "id, " +
                        "item_category_id, " +
                        "seller_id, " +
                        "name, " +
                        "bid_count, " +
                        "starting_time, " +
                        "ending_time, " +
                        "price, " +
                        "starting_price, " +
                        "buy_now_price, " +
                        "starting_quantity, " +
                        "quantity, " +
                        "location, " +
                        "state_id, " +
                        "high_bidder_id, " +
                        "postage_transfer, " +
                        "postcode, " +
                        "foto_count, " +
                        "duration, " +


                        "shipping_time_id," +

                        " p_bold_title, " +

                        "p_highlight, " +

                        "p_thumbnail, " +

                        "corporate, " +




                        "additional_pay_info, " +
                        "show_postcode, " +

                        "closed, " +
                        "payment_bank_transfer, " +
                        "payment_online, " +
                        "payment_on_delivery, " +


                        "type_id, " +
                        "quantity_type_id, " +
                        "shipment_payer_id, " +
                        "shipment_payment_description, " +
                        "create_time, create_by, " +
                        "modify_time, modify_by) VALUES (" +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItId() + ", " +
                        itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getCategoryId() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItSellerId() + ", '" +
                        StringEscapeUtils.escapeSql(doShowItemInfoExtResponse.getItemListInfoExt().getItName()) + "', " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItBidCount() + ", '" +
                        dtf.format(calculateStart(doShowItemInfoExtResponse.getItemListInfoExt().getItEndingTime(), itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getTimeToEnd())) + "', '" +
                        dtf.format(fromUnix(doShowItemInfoExtResponse.getItemListInfoExt().getItEndingTime())) + "', " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItPrice() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItStartingPrice() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItBuyNowPrice() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItStartingQuantity() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItQuantity() + ", '" +
                        StringEscapeUtils.escapeSql(doShowItemInfoExtResponse.getItemListInfoExt().getItLocation()) + "', " +
                        //tady se udela trosku podel, z doc. 1-aktivni, 2-ukoncene, 3-zavrene prodejcem, u nas 1-planovane, 2-aktivni, 3-ukoncene, takto si budou odpovidat 2 ze 3 stavu
                        ((doShowItemInfoExtResponse.getItemListInfoExt().getItEndingInfo() + 1) % 3) + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItHighBidder() + ", " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItWireTransfer() + ", '" +
                        StringEscapeUtils.escapeSql(doShowItemInfoExtResponse.getItemListInfoExt().getItPostcode()) + "', " +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItFotoCount() + ", " +
                        calculateDuration(itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getTimeToEnd()) + ", " +
                        //"?, " +
                        //"?, " +
                        //podel ale mohlo by realne po prepoctu z hodin na dny odpovidat
                        doShowItemInfoExtResponse.getItemListInfoExt().getItOrderFulfillmentTime() + ", " +
                        //"?, " +
                        (itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getPromotionInfo() % 4 == 0 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        //"?, " +
                        ((itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getPromotionInfo() / 2 == 1 || itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getPromotionInfo() / 2 == 3) ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        //"?, " +
                        //z doc. je 1 featuring
                        ((itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getPromotionInfo() % 4 == 1 || itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getPromotionInfo() % 4 == 3) ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        //"?, " +
                        //podel ale mohlo by takto fungovat
                        StringUtils.isNotBlank(doShowItemInfoExtResponse.getItemCompanyInfo().getCompanyName()) + ", " +
                        //"?, " +
                        //"?, " +
                        //"?, " +
                        //"?, " +
                        (doShowItemInfoExtResponse.getItemPaymentOptions().getPayOptionSeeDesc() == 1 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        //podel ale mohlo by takto fungovat
                        StringUtils.isNotBlank(doShowItemInfoExtResponse.getItemListInfoExt().getItPostcode()) + ", " +
                        //"?, " +
                        (doShowItemInfoExtResponse.getItemListInfoExt().getItEndingInfo() == 3 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        (doShowItemInfoExtResponse.getItemPaymentOptions().getPayOptionTransfer() == 1 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        (doShowItemInfoExtResponse.getItemPaymentOptions().getPayOptionAllegroPay() == 1 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        (doShowItemInfoExtResponse.getItemPaymentOptions().getPayOptionOnDelivery() == 1 ? Boolean.TRUE : Boolean.FALSE) + ", " +
                        //"?, " +
                        //"?, " +
                        //podel ale mohlo by takto fungovat
                        (doShowItemInfoExtResponse.getItemListInfoExt().getItBuyNowActive() == 1 ? 1 : 3) + ", " +
                        (doShowItemInfoExtResponse.getItemListInfoExt().getItQuantityType() + 1) + ", " +
                        "1, '" +//"?, '" +
                        StringEscapeUtils.escapeSql(doShowItemInfoExtResponse.getItemListInfoExt().getItPostInfo()) + "', '" +
                        dtf.format(LocalDateTime.now()) + "', 'install', '" +
                        dtf.format(LocalDateTime.now()) + "', 'install');";
                itemLines.add(itemLine);
                bwItem.write(itemLine);
                bwItem.newLine();

                //ItemDescription
                //todo tady byl problem s auditnima sloupcema
                String descriptionLine = "INSERT INTO item_desc(item_id, description, create_time, create_by, modify_time, modify_by) VALUES (" +
                        doShowItemInfoExtResponse.getItemListInfoExt().getItId() + ", '" +
                        StringEscapeUtils.escapeSql(doShowItemInfoExtResponse.getItemListInfoExt().getItDescription()) + "', '" +
                        dtf.format(LocalDateTime.now()) + "', 'install', '" +
                        dtf.format(LocalDateTime.now()) + "', 'install');";
                descriptionLines.add(descriptionLine);
                bwDescription.write(descriptionLine);
                bwDescription.newLine();

                //ItemAttributeValue
                //todo tady to obcas hazelo chyby na item id
                for (AttribStruct attribStruct : doShowItemInfoExtResponse.getItemAttribList().getItem()) {
                    for (String val : attribStruct.getAttribValues().getItem()) {
                        String valueLine = "INSERT INTO item_attribute_value(item_id, attribute_id, value, create_time, create_by, modify_time, modify_by) SELECT " +
                                doShowItemInfoExtResponse.getItemListInfoExt().getItId() + ", " +
                                "a.id, '" +
                                //z name a id attributu by se dalo vytahnout id z tabulky item_attribute_list_value
                                StringEscapeUtils.escapeSql(val) + "', '" +
                                dtf.format(LocalDateTime.now()) + "', 'install', '" +
                                dtf.format(LocalDateTime.now()) + "', 'install' " +
                                "FROM item_attribute a JOIN item_category_attribute ca on ca.atribute_id = a.id WHERE a.name = '" + attribStruct.getAttribName() + "' AND ca.category_id = " + itemIds.get(doShowItemInfoExtResponse.getItemListInfoExt().getItId()).getCategoryId() + ";";
                        valueLines.add(valueLine);
                        bwValue.write(valueLine);
                        bwValue.newLine();
                    }
                }

                //Shipping
                for (PostageStruct postageStruct : doShowItemInfoExtResponse.getItemPostageOptions().getItem()) {
                    String shippingLine = "INSERT INTO item_shipping_option(shipping_method_id, fist_package_price, next_package_price, package_quantity, free_of_charge, item_id, create_time, create_by, modify_time, modify_by) VALUES (" +
                            convertShippingMethod(postageStruct.getPostageId()) + ", " +
                            postageStruct.getPostageAmount() + ", " +
                            postageStruct.getPostageAmountAdd() + ", " +
                            postageStruct.getPostagePackSize() + ", " +
                            (postageStruct.getPostageFreeShipping() == 1 ? Boolean.TRUE : Boolean.FALSE ) + ", " +
                            doShowItemInfoExtResponse.getItemListInfoExt().getItId() + ", '" +
                            dtf.format(LocalDateTime.now()) + "', 'install', '" +
                            dtf.format(LocalDateTime.now()) + "', 'install');";
                    shippingLines.add(shippingLine);
                    bwShipping.write(shippingLine);
                    bwShipping.newLine();
                }
            }

            //zapnuti kontroly klicu
            /*
            bwAccount.write("alter table account enable trigger all;");
            bwUser.write("alter table user_ enable trigger all;");
            bwItem.write("alter table item enable trigger all;");
            bwDescription.write("alter table item_desc enable trigger all;");
            bwValue.write("alter table item_attribute_value enable trigger all;");
            bwShipping.write("alter table item_shipping_option enable trigger all;");
            */

            bwAccount.close();
            bwUser.close();
            bwItem.close();
            bwDescription.close();
            bwValue.close();
            bwShipping.close();
            System.out.println("Itemy a uzivatele vytvoreny");

            List<String> allLinesData = new ArrayList<>();
            allLinesData.addAll(accountLines);
            allLinesData.addAll(userLines);
            allLinesData.addAll(itemLines);
            allLinesData.addAll(valueLines);
            allLinesData.addAll(descriptionLines);
            allLinesData.addAll(shippingLines);

            FileUtils.writeLines(allLines, allLinesData);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    private void addUser(List<Long> userIds, Long userId, List<String> accountLines, BufferedWriter bwAccount, String login, int rating, List<String> userLines, BufferedWriter bwUser) throws IOException {
        if (!userIds.contains(userId)) {
            userIds.add(userId);

            //Account
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            String accountLine = "INSERT INTO account(status_id, type_id, web_api_key, create_time, create_by, modify_time, modify_by) VALUES (1, 1, '" +
                    randomUUIDString + "', '" +
                    dtf.format(LocalDateTime.now()) + "', 'install', '" +
                    dtf.format(LocalDateTime.now()) + "', 'install');";
            accountLines.add(accountLine);
            bwAccount.write(accountLine);
            bwAccount.newLine();

            //User
            String userLine = "INSERT INTO user_(id, login, password_hash, password_hash_old, create_date, login_date, selected_country, rating, salt, account_id, create_time, create_by, modify_time, modify_by) SELECT " +
                    userId + ", '" +
                    StringEscapeUtils.escapeSql(login) + "', " +
                    //admin
                    "'ab5ffc418ce2c4d1bf91f8f49f1f96f2f98291fe', " +
                    //admin
                    "'ab5ffc418ce2c4d1bf91f8f49f1f96f2f98291fe', '" +
                    dtf.format(LocalDateTime.now()) + "', '" +
                    dtf.format(LocalDateTime.now()) + "', " +
                    "1, " +//"?, " +
                    rating + ", " +
                    "'xZ4Gz517dD8ilkYpQpuZQQ==', " +
                    "id, '" +
                    dtf.format(LocalDateTime.now()) + "', 'install', '" +
                    dtf.format(LocalDateTime.now()) + "', 'install' FROM account WHERE web_api_key = '" + randomUUIDString + "';";
            userLines.add(userLine);
            bwUser.write(userLine);
            bwUser.newLine();
        }
    }

    private int convertShippingMethod(int oldMethodId) {
        switch(oldMethodId) {
            case 3 : {
                //Balík Do ruky
                return 1;
            }
            case 22 : {
                //Balík Na poštu
                return 2;
            }
            case 11 : {
                //Cenný balík
                return 3;
            }
            case 9 : {
                //Elektronická pošta (email)
                return 4;
            }
            case 6 : {
                //Kurýrní služba
                return 5;
            }
            case 8 : {
                //Osobní převzetí
                return 6;
            }
            case 23 : {
                //Osobní převzetí po platbě předem
                return 7;
            }
            case 7 : {
                //Dobírka
                return 9;
            }
            case 1 : {
                //Obyčejný balík
            }
            case 2 : {
                //EMS
            }
            case 4 : {
                //Obyčejné psaní
            }
            case 5 : {
                //Doporučená zásilka
            }
            case 10 : {
                //Cenné psaní
            }
            case 12 : {
                //Kurýrní služba - dobírka
            }
            case 13 : {
                //Zásilka do zahraničí
            }
            case 10141 : {
                //Výdejní místo - platba předem - AukroPoint
            }
            case 20141 : {
                //Výdejní místo - platba při převzetí - AukroPoint
            }
            default : {
                //todo doplnit chybejici metody
                return 1;
            }
        }
    }

    private LocalDateTime fromUnix(Long timestamp) {
        return LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);
    }

    private LocalDateTime calculateStart(Long unixEnding, String timeToEnd) {
        LocalDateTime end = LocalDateTime.ofEpochSecond(unixEnding, 0, ZoneOffset.UTC);
        int daysToEnd = 1;
        if(StringUtils.isNumeric(timeToEnd.split(" ")[0])) {
            daysToEnd += Integer.parseInt(timeToEnd.split(" ")[0]);
        }
        int daysAdd = getDaysAdd(daysToEnd);
        return end.minusDays(daysAdd);
    }

    private Integer calculateDuration(String timeToEnd) {
        int daysToEnd = 1;
        if(StringUtils.isNumeric(timeToEnd.split(" ")[0])) {
            daysToEnd += Integer.parseInt(timeToEnd.split(" ")[0]);
        }
        int daysAdd = getDaysAdd(daysToEnd);
        return daysAdd;
    }

    private int getDaysAdd(int daysToEnd) {
        switch (daysToEnd) {
            case 1 :
            case 2 :
            case 3 : {
                return 3;
            }
            case 4 :
            case 5 : {
                return 5;
            }
            case 6 :
            case 7 : {
                return 7;
            }
            case 8 :
            case 9 :
            case 10 :
            default : {
                return 10;
            }
        }
    }
}
