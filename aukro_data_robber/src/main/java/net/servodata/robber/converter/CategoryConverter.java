package net.servodata.robber.converter;

import allegro.wsdl.CatInfoType;
import net.servodata.robber.bo.ItemCategoryBo;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.core.convert.converter.Converter;

/**
 * @author <a href="mailto:pospisek@servodata.net">Michal Pospisek</a>
 */
public class CategoryConverter implements Converter<CatInfoType, ItemCategoryBo> {

    @Override
    public ItemCategoryBo convert(CatInfoType catInfoType) {
        ItemCategoryBo result = new ItemCategoryBo();
        result.setId(Long.valueOf(catInfoType.getCatId()));
        result.setName(StringEscapeUtils.escapeSql(catInfoType.getCatName()));
        result.setRoot(catInfoType.getCatParent() == 0);
        result.setPosition(catInfoType.getCatPosition());
        result.setParentId(catInfoType.getCatParent() == 0 ? null : Long.valueOf(catInfoType.getCatParent()));
        return result;
    }
}
