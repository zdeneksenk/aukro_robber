package net.servodata.robber.converter;

import allegro.wsdl.SellFormType;
import net.servodata.robber.bo.ItemAttributeBo;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:pospisek@servodata.net">Michal Pospisek</a>
 */
public class AttributeConverter implements Converter<SellFormType, ItemAttributeBo> {

    @Override
    public ItemAttributeBo convert(SellFormType sellFormType) {
        ItemAttributeBo result = new ItemAttributeBo();
        result.setId(Long.valueOf(sellFormType.getSellFormParamId()));
        result.setType(Long.valueOf(sellFormType.getSellFormType()));
        result.setName(StringEscapeUtils.escapeSql(sellFormType.getSellFormTitle()));
        result.setDefaultValue(String.valueOf(sellFormType.getSellFormDefValue()));
        result.setRequired(sellFormType.getSellFormOpt() == 1);
        result.setPosition(sellFormType.getSellFormPos());
        result.setLength(sellFormType.getSellFormLength());
        result.setRangeMin(new BigDecimal(StringEscapeUtils.escapeSql(sellFormType.getSellMinValue())).intValue());
        result.setRangeMax(new BigDecimal(StringEscapeUtils.escapeSql(sellFormType.getSellMaxValue())).intValue());
        result.setDescription(StringEscapeUtils.escapeSql(sellFormType.getSellFormFieldDesc()));
        result.setUnit(StringEscapeUtils.escapeSql(sellFormType.getSellFormUnit()));

        result.setCategoryId(Long.valueOf(sellFormType.getSellFormCat()));

        return result;
    }
}
