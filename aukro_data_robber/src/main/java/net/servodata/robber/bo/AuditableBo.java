package net.servodata.robber.bo;

import java.time.LocalDateTime;

public interface AuditableBo {

    String getCreatedBy();

    void setCreatedBy(String createdBy);

    LocalDateTime getCreateTime();

    void setCreateTime(LocalDateTime createTime);

    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    LocalDateTime getModifyTime();

    void setModifyTime(LocalDateTime modifyTime);

}
