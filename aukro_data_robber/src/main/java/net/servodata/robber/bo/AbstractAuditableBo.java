package net.servodata.robber.bo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractAuditableBo extends AbstractBo implements AuditableBo {

    public AbstractAuditableBo() {
        super();
        setCreateTime(LocalDateTime.now());
        setCreatedBy("install");
        setModifyTime(LocalDateTime.now());
        setModifiedBy("install");
    }

    private String createdBy;

    private LocalDateTime createTime;

    private String modifiedBy;

    private LocalDateTime modifyTime;
}
