package net.servodata.robber.bo;

import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
public class ItemCategoryBo extends AbstractAuditableBo {

    public ItemCategoryBo() {
        super();
        this.active = Boolean.TRUE;
    }

    /**
     * Primary key
     */
    private Long id;

    private String name;

    /**
     * prvni uroven kategorii
     */
    private Boolean root;

    /**
     * koncova kategorie, nema uz zadne uzly
     */
    private Boolean leaf;

    /**
     * aktivni hodnota, bude se nabizet
     */
    private Boolean active;

    /**
     * Definuje poradi kategorie na stejne urovni. Zacina od nuly.
     */
    private int position;

    private Long parentId;

    private ItemCategoryBo parent;

    public String getFullPath() {
        if(parent != null) {
            return parent.getFullPath() + " - > " + name;
        } else {
            return name;
        }
    }
}
