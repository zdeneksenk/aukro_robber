package net.servodata.robber.bo;

import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
public class ItemAttributeBo extends AbstractAuditableBo {

    public ItemAttributeBo() {
        super();
    }

    /**
     * Primary key
     */
    private Long id;


    private String name;

    /**
     * ulozeno ve formatu json
     */

    private String defaultValue;

    /**
     * Urcuje zda je povinost atribut vyplnit
     */

    private boolean required;

    /**
     * Poradi polozky v ramci zarazeni do kategorie.
     */

    private int position;

    /**
     * Povoleny pocet znaku pro textove pole
     */

    private Integer length;

    /**
     * Minimalni hodnota
     */

    private Integer rangeMin;

    /**
     * Maximalni hodnota
     */

    private Integer rangeMax;


    private String description;

    /**
     * jednotka
     */

    private String unit;

    /**
     * Typ atributu (Text, cislo, rozsah hodnot, vyber hodnoty, ...)
     */

    private Long type;




    private Long categoryId;
}
