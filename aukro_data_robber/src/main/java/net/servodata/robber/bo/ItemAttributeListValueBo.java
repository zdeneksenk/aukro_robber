package net.servodata.robber.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 */
@Getter
@Setter
public class ItemAttributeListValueBo extends AbstractAuditableBo {

    public ItemAttributeListValueBo() {
        super();
    }

    private Long id;

    /**
     * Vychozi nazev hodnoty
     */
    private String name;

    private boolean active;

    private Long attribute;
}
