package net.servodata.robber.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 */
@Getter
@Setter
public class ItemCategoryAttributeBo extends AbstractAuditableBo {

    public ItemCategoryAttributeBo() {
        super();
    }

    /**
     * Primary key
     */
    private Long id;

    private boolean mandatory;

    private boolean important;

    /**
     * Typ atributu (Text, cislo, rozsah hodnot, vyber hodnoty, ...)
     */
    private Long category;

    private Long attribute;
}
