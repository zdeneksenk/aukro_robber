package net.servodata.robber.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Denormalizacni tabule obsahujici vsechny atributy ze vsech predku.
 * 
 * - Prvotni inzert pri zalozeni nove kategorie
 * - Aktualizuje se prubezne
 * - Pozor pri mazani v predkovy - nedovoli cizi klic
 */
@Getter
@Setter
public class ItemCategoryAttributeInheritedBo extends AbstractBo {

    public ItemCategoryAttributeInheritedBo() {
        super();
    }

    private Long id;

    private boolean disable;

    private Long categoryAttribute;

    private Long category;
}
