package net.servodata.robber.bo;

/**
 * @author <a href="mailto:stepan.marek@doxologic.com">Stepan Marek</a>
 */
public interface IdentifiedBo extends IdentifiedObject {
}
